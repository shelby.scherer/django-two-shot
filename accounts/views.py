from django.shortcuts import render, redirect
from accounts.forms import LogInPage
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.models import User

# Create your views here.
def user_login(request):
    if request.method == "POST":
        form = LogInPage(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]

            user = authenticate(
                request,
                username=username,
                password=password,
            )
            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = LogInPage()
    context = {
        "form": form,
    }

    return render(request, "accounts/login.html", context)
